computational_investing
=======================

Repository for [Computational Investing on Coursera](https://class.coursera.org/compinvesting1-003/class), and a few Quant stuff in Python and [QSTK](http://wiki.quantsoftware.org/index.php?title=QuantSoftware_ToolKit)
